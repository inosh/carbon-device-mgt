package io.entgra.analytics.mgt.grafana.proxy.api.service.exception;

public class RefererNotValid extends Exception {
    public RefererNotValid(String msg) {
        super(msg);
    }
}
